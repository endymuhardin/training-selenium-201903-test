# Aplikasi untuk Test #

* [XPath Helper for Chrome](https://chrome.google.com/webstore/detail/xpath-helper/hgimnogjllphhhkhlmebbmlgjoejdpjl)
* [Tutorial Membuat Report dengan Extent Report](https://www.seleniumeasy.com/selenium-tutorials/extent-reports-using-testng-listeners)
* [Tutorial Appium](https://nishantverma.gitbooks.io/appium-for-android/content/)

## Cara menjalankan Selenium Grid Hub ##

```
java -jar selenium-server-standalone-3.141.59.jar -role hub
```

Outputnya seperti ini

```
10:47:20.388 INFO [GridLauncherV3.parse] - Selenium server version: 3.141.59, revision: e82be7d358
10:47:20.617 INFO [GridLauncherV3.lambda$buildLaunchers$5] - Launching Selenium Grid hub on port 4444
2019-07-19 10:47:21.422:INFO::main: Logging initialized @1818ms to org.seleniumhq.jetty9.util.log.StdErrLog
10:47:21.913 INFO [Hub.start] - Selenium Grid hub is up and running
10:47:21.917 INFO [Hub.start] - Nodes should register to http://192.168.1.6:4444/grid/register/
10:47:21.919 INFO [Hub.start] - Clients should connect to http://192.168.1.6:4444/wd/hub
```

## Cara menjalankan Selenium Grid Node ##

```
java -jar selenium-server-standalone-3.141.59.jar -role node -hub http://192.168.1.6:4444/grid/register/
```

Outputnya seperti ini

```
10:48:43.359 INFO [GridLauncherV3.parse] - Selenium server version: 3.141.59, revision: e82be7d358
10:48:43.633 INFO [GridLauncherV3.lambda$buildLaunchers$7] - Launching a Selenium Grid node on port 4027
2019-07-19 10:48:43.848:INFO::main: Logging initialized @1012ms to org.seleniumhq.jetty9.util.log.StdErrLog
10:48:44.416 INFO [WebDriverServlet.<init>] - Initialising WebDriverServlet
10:48:44.593 INFO [SeleniumServer.boot] - Selenium Server is up and running on port 4027
10:48:44.594 INFO [GridLauncherV3.lambda$buildLaunchers$7] - Selenium Grid node is up and ready to register to the hub
10:48:44.687 INFO [SelfRegisteringRemote$1.run] - Starting auto registration thread. Will try to register every 5000 ms.
10:48:45.392 INFO [SelfRegisteringRemote.registerToHub] - Registering the node to the hub: http://192.168.1.6:4444/grid/register
10:48:45.526 INFO [SelfRegisteringRemote.registerToHub] - The node is registered to the hub and ready to use
10:54:57.222 INFO [ActiveSessionFactory.apply] - Capabilities are: {
  "browserName": "firefox",
  "marionette": true
}
```

## Cara Setting Android ##

Agar kita bisa menjalankan aplikasi yang mau ditest di android, kita perlu mengaktifkan developer mode.

[![Developer Options](docs/01-developer-options.png)](docs/01-developer-options.png)

Selanjutnya, matikan verifikasi aplikasi yang diinstal melalui USB

[![Verify USB Apps](docs/02-disable-usb-verification.png)](docs/02-disable-usb-verification.png)

Pada waktu handphone dihubungkan dengan komputer, gunakan mode koneksi USB file atau image transfer

[![Image/File Transfer](docs/03-usb-mode-file-image.png)](docs/03-usb-mode-file-image.png)