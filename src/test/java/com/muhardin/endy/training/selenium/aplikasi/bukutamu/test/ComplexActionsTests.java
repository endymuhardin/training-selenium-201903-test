package com.muhardin.endy.training.selenium.aplikasi.bukutamu.test;

import java.util.Calendar;
import java.util.List;
import java.util.concurrent.TimeUnit;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.interactions.Actions;

public class ComplexActionsTests {

    private static WebDriver webDriver;

    @BeforeClass
    public static void bukaBrowser() {
        ChromeOptions opsi = new ChromeOptions();
        //opsi.setHeadless(true); // tidak usah tampilkan browsernya
        webDriver = new ChromeDriver(opsi);
    }

    @AfterClass
    public static void tutupBrowser() {
        webDriver.quit();
    }

    @Test
    public void testMouseOver() throws InterruptedException {
        //Launching sample website
        webDriver.get("http://artoftesting.com/sampleSiteForSelenium.html");

        //Mouseover on submit button
        Actions action = new Actions(webDriver);
        WebElement btn = webDriver.findElement(By.id("idOfButton"));
        action.moveToElement(btn).perform();

        //Thread.sleep just for user to notice the event
        Thread.sleep(3000);

        Assert.assertTrue(webDriver.getPageSource().contains("Hovering over me"));
    }

    // Sumber : https://www.guru99.com/handling-date-time-picker-using-selenium.html
    @Test
    public void testComplexDatePicker() throws Exception{
        String dateTime = "12/07/2014 2:00 PM";

        webDriver.get("http://demos.telerik.com/kendo-ui/datetimepicker/index");

        webDriver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

        // click dulu Accept Cookies
        webDriver.findElement(By.xpath("//button[contains(., 'Accept Cookies')]")).click();
        
        //button to open calendar
        WebElement selectDate = webDriver.findElement(By.xpath("//span[@aria-controls='datetimepicker_dateview']"));

        selectDate.click();

        //button to move next in calendar
        WebElement nextLink = webDriver.findElement(By.xpath("//div[@id='datetimepicker_dateview']//div[@class='k-header']//a[contains(@class,'k-nav-next')]"));

        //button to click in center of calendar header
        WebElement midLink = webDriver.findElement(By.xpath("//div[@id='datetimepicker_dateview']//div[@class='k-header']//a[contains(@class,'k-nav-fast')]"));

        //button to move previous month in calendar
        WebElement previousLink = webDriver.findElement(By.xpath("//div[@id='datetimepicker_dateview']//div[@class='k-header']//a[contains(@class,'k-nav-prev')]"));

        //Split the date time to get only the date part
        String date_dd_MM_yyyy[] = (dateTime.split(" ")[0]).split("/");

        //get the year difference between current year and year to set in calander
        int yearDiff = Integer.parseInt(date_dd_MM_yyyy[2]) - Calendar.getInstance().get(Calendar.YEAR);

        midLink.click();

        if (yearDiff != 0) {

            //if you have to move next year
            if (yearDiff > 0) {

                for (int i = 0; i < yearDiff; i++) {

                    System.out.println("Year Diff->" + i);

                    nextLink.click();

                }

            } //if you have to move previous year
            else if (yearDiff < 0) {

                for (int i = 0; i < (yearDiff * (-1)); i++) {

                    System.out.println("Year Diff->" + i);

                    previousLink.click();

                }

            }

        }

        Thread.sleep(1000);

        //Get all months from calendar to select correct one
        List<WebElement> list_AllMonthToBook = webDriver.findElements(By.xpath("//div[@id='datetimepicker_dateview']//table//tbody//td[not(contains(@class,'k-other-month'))]"));

        list_AllMonthToBook.get(Integer.parseInt(date_dd_MM_yyyy[1]) - 1).click();

        Thread.sleep(1000);

        //get all dates from calendar to select correct one
        List<WebElement> list_AllDateToBook = webDriver.findElements(By.xpath("//div[@id='datetimepicker_dateview']//table//tbody//td[not(contains(@class,'k-other-month'))]"));

        list_AllDateToBook.get(Integer.parseInt(date_dd_MM_yyyy[0]) - 1).click();

        ///FOR TIME
        WebElement selectTime = webDriver.findElement(By.xpath("//span[@aria-controls='datetimepicker_timeview']"));

        //click time picker button
        selectTime.click();

        //get list of times
        List<WebElement> allTime = webDriver.findElements(By.xpath("//div[@data-role='popup'][contains(@style,'display: block')]//ul//li[@role='option']"));

        dateTime = dateTime.split(" ")[1] + " " + dateTime.split(" ")[2];

        //select correct time
        for (WebElement webElement : allTime) {

            if (webElement.getText().equalsIgnoreCase(dateTime)) {

                webElement.click();

            }

        }
    }
}
