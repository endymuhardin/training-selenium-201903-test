package com.muhardin.endy.training.selenium.aplikasi.bukutamu.test;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.service.local.AppiumDriverLocalService;
import java.net.URL;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.DesiredCapabilities;

public class AppiumDemoTests {
    
    private static final String URL_APPIUM_SERVER = "http://192.168.1159:4723/wd/hub";
    private static AppiumDriverLocalService service;
    private static AndroidDriver<WebElement> driver;
    
    @BeforeClass
    public static void startAppium() throws Exception {
        //service = AppiumDriverLocalService.buildDefaultService();
        //service.start();
        
        DesiredCapabilities capabilities = new DesiredCapabilities();
        capabilities.setCapability("deviceName", "emulator-5554");
        capabilities.setCapability("automatioName", "UiAutomator2");
        capabilities.setCapability("app", "/Users/endymuhardin/Downloads/ApiDemos-debug.apk");
        capabilities.setCapability("appActivity", ".ApiDemos");
        driver = new AndroidDriver<>(new URL(URL_APPIUM_SERVER), capabilities);
    }
    
    @AfterClass
    public static void stopAppium(){
        //service.stop();
    }
    
    @Test
    public void testKlikPreference(){
        MobileElement el2 = (MobileElement) driver.findElementByAccessibilityId("Preference");
        el2.click();
        driver.navigate().back();
    }
    
}
