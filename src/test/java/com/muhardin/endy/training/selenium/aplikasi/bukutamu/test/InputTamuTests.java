package com.muhardin.endy.training.selenium.aplikasi.bukutamu.test;

import com.github.javafaker.Faker;
import java.io.File;
import java.util.Locale;
import java.util.UUID;
import org.apache.commons.io.FileUtils;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class InputTamuTests {
    private static WebDriver webDriver;
    private static final String URL_APLIKASI = "https://training-selenium-201903.herokuapp.com";
    
    @BeforeClass
    public static void bukaBrowser(){
        webDriver = new ChromeDriver();
    }
    
    
    @AfterClass
    public static void tutupBrowser(){
        webDriver.quit();
    }
    
    @Test
    public void testInputNormal() throws Exception {
        System.out.println("Test input tamu");
        
        Faker f = new Faker(new Locale("id", "id"));
        
        String namaDepan = f.name().firstName();
        String namaBelakang = f.name().lastName();
        String email = f.internet().emailAddress(namaDepan.toLowerCase()+"."+namaBelakang.toLowerCase());
        String noHp = f.phoneNumber().cellPhone();
        String alamat = f.address().fullAddress();
        
        webDriver.get(URL_APLIKASI + "/tamu/form");
        
        WebElement inputNama = webDriver.findElement(By.name("nama"));
        inputNama.sendKeys(namaDepan + " "+ namaBelakang);
        
        WebElement inputEmail = webDriver.findElement(By.name("email"));
        inputEmail.sendKeys(email);
        
        WebElement inputHp = webDriver.findElement(By.name("noHp"));
        inputHp.sendKeys(noHp);
        
        WebElement inputAlamat = webDriver.findElement(By.name("alamat"));
        inputAlamat.sendKeys(alamat);
        
        Thread.sleep(3 * 1000);
        // sebelum simpan, screenshot dulu
        File screenshotSebelum = ((TakesScreenshot)webDriver).getScreenshotAs(OutputType.FILE);
        String namafile = UUID.randomUUID().toString();
        String folderScreenshot = "target"+File.separator+"screenshots";
        new File(folderScreenshot).mkdirs();
        FileUtils.copyFile(screenshotSebelum, new File(folderScreenshot+File.separator+namafile+"-01-before.png"));
        
        WebElement tombolSimpan = webDriver.findElement(By.name("simpan"));
        tombolSimpan.click();
        
        // tunggu sampai muncul title Data Tamu
        new WebDriverWait(webDriver, 10).until(ExpectedConditions.titleIs("Data Tamu"));
        
        // email yang barusan diinput ada di halaman Data Tamu
        Boolean emailAda = webDriver.getPageSource().contains(email);
        System.out.println("Email ada ? "+emailAda);
        Assert.assertTrue(webDriver.getPageSource().contains(email));
        
        // screenshot lagi hasil akhirnya
        File screenshotSetelah = ((TakesScreenshot)webDriver).getScreenshotAs(OutputType.FILE);
        FileUtils.copyFile(screenshotSetelah, new File(folderScreenshot+File.separator+namafile+"-02-after.png"));
    }
}
