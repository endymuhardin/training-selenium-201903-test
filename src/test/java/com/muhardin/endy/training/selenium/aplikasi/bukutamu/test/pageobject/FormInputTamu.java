package com.muhardin.endy.training.selenium.aplikasi.bukutamu.test.pageobject;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

public class FormInputTamu {
    
    @FindBy(name = "nama") private WebElement inputNama;
    @FindBy(name = "email") private WebElement inputEmail;
    @FindBy(name = "noHp") private WebElement inputHp;
    @FindBy(name = "alamat") private WebElement inputAlamat;
    @FindBy(name = "simpan") private WebElement tombolSimpan;
    @FindBy(name = "tanggalLahir") private WebElement inputTanggalLahir;
    @FindBy(name = "pendidikan") private WebElement comboPendidikan;
    @FindBy(id = "jenisKelamin1") private WebElement radioPria;
    @FindBy(id = "jenisKelamin2") private WebElement radioWanita;
    
    private Select selectPendidikan;

    public FormInputTamu(WebDriver wd){
        PageFactory.initElements(wd, this);
        selectPendidikan = new Select(comboPendidikan);
    }
    
    public void isiNama(String nama){
        inputNama.sendKeys(nama);
    }
    
    public void isiEmail(String email){
        inputEmail.sendKeys(email);
    }
    
    public void isiHp(String hp){
        inputHp.sendKeys(hp);
    }
    
    public void isiAlamat(String alamat){
        inputAlamat.sendKeys(alamat);
    }
    
    public void isiTanggalLahir(String tanggalLahir){
        inputTanggalLahir.sendKeys(tanggalLahir);
    }
    
    public void pilihJenisKelamin(String jenisKelamin){
        if("PRIA".equalsIgnoreCase(jenisKelamin.trim())){
            radioPria.click();
        }
        
        if("WANITA".equalsIgnoreCase(jenisKelamin.trim())){
            radioWanita.click();
        }
    }
    
    public void pilihPendidikan(String pendidikan){
        if("SD".equalsIgnoreCase(pendidikan.trim())){
            selectPendidikan.selectByValue("SD");
        }
        
        if("SMP".equalsIgnoreCase(pendidikan.trim())){
            selectPendidikan.selectByValue("SMP");
        }
        
        if("SMK".equalsIgnoreCase(pendidikan.trim())){
            selectPendidikan.selectByValue("SMK");
        }
        
        if("SMA".equalsIgnoreCase(pendidikan.trim())){
            selectPendidikan.selectByValue("SMA");
        }
    }
    
    public void simpanData(){
        tombolSimpan.click();
    }
}
