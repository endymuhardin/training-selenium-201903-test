package com.muhardin.endy.training.selenium.aplikasi.bukutamu.test.helper;

import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;

public class SeleniumTestHelper {
    public static void screenshot(WebDriver webDriver, String namafile){
        try {
            File screenshotSebelum = ((TakesScreenshot)webDriver).getScreenshotAs(OutputType.FILE);
            String folderScreenshot = "target"+File.separator+"screenshots";
            new File(folderScreenshot).mkdirs();
            FileUtils.copyFile(screenshotSebelum, 
                    new File(folderScreenshot+File.separator+namafile+".png"));
        } catch (IOException ex) {
            Logger.getLogger(SeleniumTestHelper.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
