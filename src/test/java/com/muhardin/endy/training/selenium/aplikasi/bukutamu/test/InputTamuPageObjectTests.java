package com.muhardin.endy.training.selenium.aplikasi.bukutamu.test;

import com.github.javafaker.Faker;
import com.muhardin.endy.training.selenium.aplikasi.bukutamu.test.helper.SeleniumTestHelper;
import com.muhardin.endy.training.selenium.aplikasi.bukutamu.test.pageobject.FormInputTamu;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
import junitparams.FileParameters;
import junitparams.JUnitParamsRunner;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.By;
import org.openqa.selenium.Platform;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

@RunWith(JUnitParamsRunner.class)
public class InputTamuPageObjectTests {

    private static WebDriver webDriver;

    private static final String URL_APLIKASI = "https://training-selenium-201903.herokuapp.com";
    private static final String URL_SELENIUM_HUB = "http://192.168.1.159:12345/wd/hub";
    private static final Integer timeout = 60;

    public static void bukaBrowserLocal() {
        ChromeOptions opsi = new ChromeOptions();
        //opsi.setHeadless(true); // tidak usah tampilkan browsernya
        webDriver = new ChromeDriver(opsi);
    }
    
    @BeforeClass
    public static void bukaBrowserRemote(){
        try {
            DesiredCapabilities capabilities = new DesiredCapabilities();
            //capabilities.setBrowserName("safari");
            capabilities.setPlatform(Platform.MAC);
            
            webDriver = new RemoteWebDriver(new URL(URL_SELENIUM_HUB), capabilities);
            webDriver.manage().timeouts().implicitlyWait(timeout, TimeUnit.SECONDS);
        } catch (MalformedURLException ex) {
            Logger.getLogger(InputTamuPageObjectTests.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @AfterClass
    public static void tutupBrowser() {
        webDriver.quit();
    }

    @Before
    public void resetDatabaseTamu() throws Exception {
        URL url = new URL(URL_APLIKASI + "/tamu/reset");
        HttpURLConnection httpCon = (HttpURLConnection) url.openConnection();
        httpCon.setDoOutput(true);
        httpCon.setRequestMethod("DELETE");
        httpCon.connect();
        httpCon.getResponseCode();
    }

    @Test
    public void testInputNormal() throws Exception {
        String namafile = LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyyMMdd-HHmmss"))
                + "-form-input-tamu";

        Faker f = new Faker(new Locale("id", "id"));

        String namaDepan = f.name().firstName();
        String namaBelakang = f.name().lastName();
        String email = f.internet().emailAddress(namaDepan.toLowerCase() + "." + namaBelakang.toLowerCase());
        String noHp = f.phoneNumber().cellPhone();
        String alamat = f.address().fullAddress();

        webDriver.get(URL_APLIKASI + "/tamu/form");

        FormInputTamu fi = new FormInputTamu(webDriver);
        fi.isiNama(namaDepan + " " + namaBelakang);
        fi.isiEmail(email);
        fi.isiHp(noHp);
        fi.isiAlamat(alamat);

        // untuk pengisian tanggal dengan datepicker yang rumit, bisa dibaca di sini:
        // https://www.guru99.com/handling-date-time-picker-using-selenium.html
        fi.isiTanggalLahir(f.date().birthday(20, 80) // generate birthday dari Faker
                .toInstant().atZone(ZoneId.systemDefault()).toLocalDate()
                .format(DateTimeFormatter.ISO_DATE)); // konversi Date menjadi LocalDate
        fi.pilihJenisKelamin("wanita");
        fi.pilihPendidikan(" smk ");

        SeleniumTestHelper.screenshot(webDriver, namafile + "-01-before-save");

        fi.simpanData();

        // tunggu sampai muncul title Data Tamu
        new WebDriverWait(webDriver, timeout).until(ExpectedConditions.titleIs("Data Tamu"));

        // email yang barusan diinput ada di halaman Data Tamu
        Boolean emailAda = webDriver.getPageSource().contains(email);
        Assert.assertTrue(webDriver.getPageSource().contains(email));

        SeleniumTestHelper.screenshot(webDriver, namafile + "-02-after-save");
    }

    @FileParameters("classpath:sample-tamu.csv")
    @Test
    public void testInputError(String nama, String email, String noHp, String alamat,
            String tanggalLahir, String jenisKelamin, String pendidikan,
            Boolean sukses, String fieldError, String pesanError) throws Exception {
        String namafile = LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyyMMdd-HHmmss"))
                + "-form-input-tamu";

        webDriver.get(URL_APLIKASI + "/tamu/form");

        FormInputTamu fi = new FormInputTamu(webDriver);
        fi.isiNama(nama);
        fi.isiEmail(email);
        fi.isiHp(noHp);
        fi.isiAlamat(alamat);
        fi.isiTanggalLahir(tanggalLahir);
        fi.pilihJenisKelamin(jenisKelamin);
        fi.pilihPendidikan(pendidikan);
        SeleniumTestHelper.screenshot(webDriver, namafile + "-01-before-save");
        fi.simpanData();
        
        if (sukses) {
            // tunggu sampai muncul title Data Tamu
            new WebDriverWait(webDriver, timeout).until(ExpectedConditions.titleIs("Data Tamu"));

            // email yang barusan diinput ada di halaman Data Tamu
            Assert.assertTrue(webDriver.getPageSource().contains(email));
        } else {
            // tunggu sampai muncul title Data Tamu
            new WebDriverWait(webDriver, timeout).until(ExpectedConditions.titleIs("Edit Tamu"));

            // pastikan ada div yang berisi pesan error untuk field yang invalid
            String xpathFieldError = "//*[@id='pesan-error-" + fieldError + "']";
            new WebDriverWait(webDriver, timeout).until(ExpectedConditions.presenceOfElementLocated(By.xpath(xpathFieldError)));
            
            List<WebElement> divPesanError = webDriver.findElements(By.xpath(xpathFieldError));

            Assert.assertFalse("Div pesan error untuk field " + fieldError + " tidak ditemukan", divPesanError.isEmpty());
            Assert.assertTrue("Div pesan error cuma boleh satu per field", divPesanError.size() == 1);

            WebElement elemenPesanError = divPesanError.get(0);
            Assert.assertTrue(elemenPesanError.getText().contains(pesanError));
        }

        SeleniumTestHelper.screenshot(webDriver, namafile + "-02-after-save");
    }
}
